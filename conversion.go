package rhelper

import (
	"math"
	"strconv"
)

func ToInt64(from string, def int64) (res int64) {
	var err error

	res, err = strconv.ParseInt(from, 10, 64)
	if err != nil {
		res = def
		return
	}

	return
}

func ToInt(from string, deflt int) int {
	return ToIntWithMin(from, deflt, math.MinInt)
}

func ToIntWithMin(from string, deflt, min int) int {
	return ToIntWithMinMax(from, deflt, min, math.MaxInt)
}

func ToIntWithMax(from string, deflt, max int) int {
	return ToIntWithMinMax(from, deflt, math.MinInt, max)
}

func ToIntWithMinMax(from string, deflt, min, max int) int {
	to, err := strconv.Atoi(from)
	if err != nil {
		to = deflt
	}

	if to < min && min != math.MinInt {
		to = min
	}

	if to > max && max != math.MaxInt {
		to = max
	}

	return to
}

func ToIntFromBool(b bool, t, f int) int {
	if b {
		return t
	}

	return f
}

func ToFloat64(src string, def float64) (res float64) {
	var err error

	res, err = strconv.ParseFloat(src, 64)
	if err != nil {
		res = def
	}

	return
}

func ToFloat32(src string, def float32) (res float32) {
	var err error
	var res64 float64

	res64, err = strconv.ParseFloat(src, 32)
	if err != nil {
		res = def
		return
	}

	res = float32(res64)
	return
}

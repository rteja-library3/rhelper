package rhelper

import (
	"math/rand"
	"time"
)

func GenerateRandom(min, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min+1) + min
}

func GenerateRandomString(letters string, charlen int) (res string) {
	mxLetterLen := len(letters) - 1

	for i := 0; i < charlen; i++ {
		res += string(letters[GenerateRandom(0, mxLetterLen)])
	}

	return
}

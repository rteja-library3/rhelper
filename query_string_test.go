package rhelper_test

import (
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
)

func TestQueryString(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "10")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryString(request, "limit")

	exp := "10"

	assert.Equal(t, exp, res, "[TestQueryString] Result should \"10\"")
}

func TestQueryStrings(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("city[]", "10")
	q.Add("city[]", "11")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStrings(request, "city[]")

	exp := []string{"10", "11"}

	assert.Equal(t, exp, res, "[TestQueryStrings] Result should []string{\"10\", \"11\"}")
}

func TestQueryStringToInts(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("city[]", "10")
	q.Add("city[]", "11")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToInts(request, "city[]", -1)

	exp := []int{10, 11}

	assert.Equal(t, exp, res, "[TestQueryStringToInts] Result should []int{10, 11}")
}

func TestQueryStringToInt(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "10")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToInt(request, "limit", 5)

	exp := int(10)

	assert.Equal(t, exp, res, "[TestQueryStringToInt] Result should int(10)")
}

func TestQueryStringToInt64(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "10")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToInt64(request, "limit", 5)

	exp := int64(10)

	assert.Equal(t, exp, res, "[TestQueryStringToInt64] Result should int64(10)")
}

func TestQueryStringToFloat64(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "10")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToFloat64(request, "limit", 5)

	exp := float64(10)

	assert.Equal(t, exp, res, "[TestQueryStringToFloat64] Result should float64(10)")
}

func TestQueryStringToFloat64Ptr(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "10")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToFloat64Ptr(request, "limit")

	exp := float64(10)

	assert.Equal(t, &exp, res, "[TestQueryStringToFloat64Ptr] Result should float64(10)")
}

func TestQueryStringToFloat64PtrInvalid(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToFloat64Ptr(request, "limit")

	assert.Nil(t, res, "[TestQueryStringToFloat64PtrInvalid] Result should nil")
}

func TestQueryStringToBoolEmpty(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToBool(request, "limit")

	assert.Equal(t, false, res, "[TestQueryStringToBoolEmpty] Result should false")
}

func TestQueryStringToBoolInvalid(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "a")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToBool(request, "limit")

	assert.Equal(t, false, res, "[TestQueryStringToBoolInvalid] Result should false")
}

func TestQueryStringToBoolValid(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "t")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToBool(request, "limit")

	assert.Equal(t, true, res, "[TestQueryStringToBoolValid] Result should true")
}

func TestQueryStringToPointerBoolInvalid(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "a")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToPointerBool(request, "limit")

	assert.Nil(t, res, "[TestQueryStringToPointerBoolInvalid] Result should nil")
}

func TestQueryStringToPointerBoolEmpty(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToPointerBool(request, "limit")

	assert.Nil(t, res, "[TestQueryStringToPointerBoolEmpty] Result should nil")
}

func TestQueryStringToPointerBool(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "1")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToPointerBool(request, "limit")

	exp := true

	assert.Equal(t, &exp, res, "[TestQueryStringToPointerBool] Result should true")
}

func TestQueryStringToTime(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "", nil)

	q := request.URL.Query()
	q.Add("limit", "2006-01-02T15:04:05Z")

	request.URL.RawQuery = q.Encode()

	res := rhelper.QueryStringToTime(request, "limit")

	exp := time.Date(2006, time.January, 2, 15, 4, 5, 0, time.UTC)

	assert.Equal(t, exp, res, "[TestQueryStringToTime] Result should time.Date(2006, time.January, 2, 15, 4, 5, 0, time.UTC)")
}

package rhelper

import (
	"time"
)

var (
	TimeLocationUTC     *time.Location = time.UTC
	TimeLocationJakarta *time.Location = LoadTimeLocation("Asia/Jakarta")
)

func LoadTimeLocation(loc string) (l *time.Location) {
	var err error

	l, err = time.LoadLocation(loc)
	if err != nil {
		l = TimeLocationUTC
		return
	}

	return
}

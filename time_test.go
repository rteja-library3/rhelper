package rhelper_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
)

func TestToTimeIntWithLocation(t *testing.T) {
	tm := time.Date(2010, time.December, 25, 10, 0, 0, 0, time.UTC)

	res := rhelper.ToTimeIntWithLocation(tm, nil)

	assert.Equal(t, int64(1293271200000000000), res, "[TestToTimeIntWithLocation] Result should 1293271200000000000")
}

func TestToTimeStringWithLocation(t *testing.T) {
	tm := time.Date(2010, time.December, 25, 10, 0, 0, 0, time.UTC)

	res := rhelper.ToTimeStringWithLocation(tm, nil)

	assert.Equal(t, "1293271200000000000", res, "[TestToTimeIntWithLocation] Result should \"1293271200000000000\"")
}

func TestMustParseTimeWithLocationSuccess(t *testing.T) {
	exp := time.Date(2006, time.December, 25, 0, 0, 0, 0, time.UTC)

	res := rhelper.MustParseTimeWithLocation("2006-12-25", "2006-01-02", time.UTC)

	assert.Equal(t, exp, res, "[TestMustParseTimeWithLocationSuccess] Result should same")
	assert.NotEmpty(t, res, "[TestMustParseTimeWithLocationSuccess] Result should not empty")
}

func TestMustParseTimeWithLocationEmpty(t *testing.T) {
	exp := time.Time{}

	res := rhelper.MustParseTimeWithLocation("2006-12-25", "2006", time.UTC)

	assert.Equal(t, exp, res, "[TestMustParseTimeWithLocationEmpty] Result should same")
	assert.Empty(t, res, "[TestMustParseTimeWithLocationEmpty] Result should empty")
}

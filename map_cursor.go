package rhelper

import (
	"time"

	"go.mongodb.org/mongo-driver/bson"
)

func ToMapCursor(mapCursor map[string]bson.M) (mongoFilter bson.M) {
	mongoFilter = bson.M{}

	for k, v := range mapCursor {
		fl := bson.M{}

		for key, val := range v {
			switch t := val.(type) {
			case string:
				tm, err := time.Parse(time.RFC3339, t)
				if err != nil {
					val = t
				} else {
					val = tm
				}
			default:
				val = t
			}

			fl[key] = val
		}

		mongoFilter[k] = fl
	}

	return
}

package rhelper_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
)

func TestStringOrFound(t *testing.T) {
	s := rhelper.StringOr("aa", "")

	assert.Equal(t, "aa", s, "[TestStringOrFound] Result Should \"aa\"")
}

func TestStringOrNotFound(t *testing.T) {
	s := rhelper.StringOr("", "")

	assert.Equal(t, "", s, "[TestStringOrNotFound] Result Should \"\"")
	assert.Empty(t, s, "[TestStringOrNotFound] Result should empty")
}

func TestGetAlphanumeric(t *testing.T) {
	s := rhelper.GetAlphanumeric("aa0@gmail.com")

	assert.Equal(t, "aa0gmailcom", s, "[TestGetAlphanumeric] Result Should \"aagmailcom\"")
}

module gitlab.com/rteja-library3/rhelper

go 1.17

require (
	github.com/go-chi/chi v1.5.4
	github.com/google/uuid v1.3.0
	github.com/oklog/ulid/v2 v2.0.2
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	go.mongodb.org/mongo-driver v1.8.3
)

require gitlab.com/rteja-library3/rencryption v0.0.0-20220215103608-74b4d02b0678

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

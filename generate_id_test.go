package rhelper_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
)

func TestGenerateProductCode(t *testing.T) {
	res := rhelper.GenerateProductCode("test", "id")

	assert.NotEmpty(t, res, "[TestGenerateProductCode] Result should not empty")
}

func TestGenerateSessionID(t *testing.T) {
	res := rhelper.GenerateSessionID("test@aa.com", "test", "test")

	assert.NotEmpty(t, res, "[TestGenerateSessionID] Result should not empty")
}

func TestGenerateObjectID(t *testing.T) {
	res := rhelper.GenerateObjectID("test@aa.com", "test", "test")

	assert.NotEmpty(t, res, "[TestGenerateObjectID] Result should not empty")
}

func TestGenerateULID(t *testing.T) {
	res := rhelper.GenerateULID("test@aa.com", "test", "test")

	assert.NotEmpty(t, res, "[TestGenerateULID] Result should not empty")
}

func TestGenerateTranscationID(t *testing.T) {
	res := rhelper.GenerateTranscationID(time.Now(), "test@aa.com", "test", "test", 6)

	assert.NotEmpty(t, res, "[TestGenerateTranscationID] Result should not empty")
}

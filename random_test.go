package rhelper_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
)

func TestGenerateRandom(t *testing.T) {
	res := rhelper.GenerateRandom(1, 1)

	exp := 1

	assert.Equal(t, exp, res, "[TestGenerateRandom] Result should same")
	assert.NotEmpty(t, res, "[TestGenerateRandom] Result should not empty")
}

func TestGenerateRandomString(t *testing.T) {
	res := rhelper.GenerateRandomString("aaaaa", 3)

	exp := "aaa"

	assert.Equal(t, exp, res, "[TestGenerateRandomString] Result should same")
	assert.NotEmpty(t, res, "[TestGenerateRandomString] Result should not empty")
}

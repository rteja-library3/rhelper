package rhelper

func SearchInArrayString(ars []string, s string) (pos int) {
	pos = -1
	if len(ars) == 0 {
		return
	}

	l := 0
	r := len(ars) - 1

	for l <= r {
		if ars[l] == s {
			pos = l
			return
		}

		if ars[r] == s {
			pos = r
			return
		}

		l++
		r--
	}

	return
}

func IsArrayStringExist(ars []string, s string) (b bool) {
	return SearchInArrayString(ars, s) != -1
}

package rhelper_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
)

func TestLoadTimeLocationValid(t *testing.T) {
	loc := rhelper.LoadTimeLocation("Asia/Jakarta")

	assert.NotNil(t, loc, "[TestLoadTimeLocationValid] Location should not nil")
	assert.Equal(t, rhelper.TimeLocationJakarta, loc, "[TestLoadTimeLocationValid] Location should jakarta")
}

func TestLoadTimeLocationInvalid(t *testing.T) {
	loc := rhelper.LoadTimeLocation("Asia/Hanoi")

	assert.NotNil(t, loc, "[TestLoadTimeLocationInvalid] Location should not nil")
	assert.Equal(t, rhelper.TimeLocationUTC, loc, "[TestLoadTimeLocationInvalid] Location should utc / default")
}

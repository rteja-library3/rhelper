package rhelper_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
)

func TestMinInt64(t *testing.T) {
	res := rhelper.MinInt64(1, 2, 4)

	exp := int64(1)

	assert.Equal(t, exp, res, "[TestMinInt64] Result should int64(1)")
}

func TestMinInt64Empty(t *testing.T) {
	res := rhelper.MinInt64()

	exp := int64(0)

	assert.Equal(t, exp, res, "[TestMinInt64Empty] Result should int64(0)")
}

func TestMaxInt64(t *testing.T) {
	res := rhelper.MaxInt64(1, 2, 4)

	exp := int64(4)

	assert.Equal(t, exp, res, "[TestMaxInt64] Result should int64(4)")
}

func TestMaxInt64Empty(t *testing.T) {
	res := rhelper.MaxInt64()

	exp := int64(0)

	assert.Equal(t, exp, res, "[TestMaxInt64Empty] Result should int64(0)")
}

func TestMaxInt(t *testing.T) {
	res := rhelper.MaxInt(1, 2, 4)

	exp := int(4)

	assert.Equal(t, exp, res, "[TestMaxInt] Result should int(4)")
}

func TestMaxIntEmpty(t *testing.T) {
	res := rhelper.MaxInt()

	exp := int(0)

	assert.Equal(t, exp, res, "[TestMaxIntEmpty] Result should int(0)")
}

func TestMinInt(t *testing.T) {
	res := rhelper.MinInt(1, 2, 4)

	exp := int(1)

	assert.Equal(t, exp, res, "[TestMinInt] Result should int(1)")
}

func TestMinIntEmpty(t *testing.T) {
	res := rhelper.MinInt()

	exp := int(0)

	assert.Equal(t, exp, res, "[TestMinIntEmpty] Result should int(0)")
}

func TestMinFloat64(t *testing.T) {
	res := rhelper.MinFloat64(1, 2, 4)

	exp := float64(1)

	assert.Equal(t, exp, res, "[TestMinFloat64] Result should float64(1)")
}

func TestMinFloat64Empty(t *testing.T) {
	res := rhelper.MinFloat64()

	exp := float64(0)

	assert.Equal(t, exp, res, "[TestMinFloat64Empty] Result should float64(0)")
}

func TestMaxFloat64(t *testing.T) {
	res := rhelper.MaxFloat64(1, 2, 4)

	exp := float64(4)

	assert.Equal(t, exp, res, "[TestMaxFloat64] Result should float64(4)")
}

func TestMaxFloat64Empty(t *testing.T) {
	res := rhelper.MaxFloat64()

	exp := float64(0)

	assert.Equal(t, exp, res, "[TestMaxFloat64Empty] Result should float64(0)")
}

package rhelper

import (
	"net/mail"
	"strconv"
)

func IsEmailValid(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

func IsPhoneValid(phone string) bool {
	_, err := strconv.Atoi(phone)
	return err == nil
}

func IsPhoneCodeValid(phoneCode string) bool {
	code, err := strconv.Atoi(phoneCode)
	if err != nil {
		return false
	}

	if code < 1 || code > 441624 {
		return false
	}

	return true
}

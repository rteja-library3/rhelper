package rhelper

import (
	"go.mongodb.org/mongo-driver/bson"
)

func ToMongoSort(sorts []string) (mongoSort bson.D) {
	mongoSort = make(bson.D, len(sorts))
	for line, sort := range sorts {
		isDesc := sort[0] == '-'
		if isDesc {
			sort = sort[1:]
		}

		mongoSort[line] = bson.E{
			Key:   sort,
			Value: ToIntFromBool(isDesc, -1, 1),
		}
	}

	return
}

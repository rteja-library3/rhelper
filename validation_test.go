package rhelper_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
)

func TestIsEmailValidTrue(t *testing.T) {
	email := "aa@aa.com"

	isValid := rhelper.IsEmailValid(email)

	assert.Equal(t, true, isValid, "[TestIsEmailValidTrue] Should valid email")
}

func TestIsEmailValidFalse(t *testing.T) {
	email := "aaaa.com"

	isValid := rhelper.IsEmailValid(email)

	assert.Equal(t, false, isValid, "[TestIsEmailValidTrue] Should invalid email")
}

func TestIsPhoneValidTrue(t *testing.T) {
	phone := "819923"

	isValid := rhelper.IsPhoneValid(phone)

	assert.Equal(t, true, isValid, "[TestIsPhoneValidTrue] Should valid phone")
}

func TestIsPhoneValidFalse(t *testing.T) {
	phone := "aa"

	isValid := rhelper.IsPhoneValid(phone)

	assert.Equal(t, false, isValid, "[TestIsPhoneValidFalse] Should invalid phone")
}

func TestIsPhoneCodeValidTrue(t *testing.T) {
	phone := "62"

	isValid := rhelper.IsPhoneCodeValid(phone)

	assert.Equal(t, true, isValid, "[TestIsPhoneCodeValidTrue] Should valid phone code")
}

func TestIsPhoneCodeValidFalse(t *testing.T) {
	phone := "aa"

	isValid := rhelper.IsPhoneCodeValid(phone)

	assert.Equal(t, false, isValid, "[TestIsPhoneCodeValidTrue] Should invalid phone code")
}

func TestIsPhoneCodeInvalid(t *testing.T) {
	phone := "-1"

	isValid := rhelper.IsPhoneCodeValid(phone)

	assert.Equal(t, false, isValid, "[TestIsPhoneCodeValidTrue] Should invalid phone code")
}

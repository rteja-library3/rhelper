package rhelper_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
)

func TestChiURLParamToString(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/9908-test", nil)

	ctx := context.WithValue(request.Context(), chi.RouteCtxKey, &chi.Context{
		URLParams: chi.RouteParams{
			Keys:   []string{"id"},
			Values: []string{"9908-test"},
		},
	})

	res := rhelper.ChiURLParamToString(request.WithContext(ctx), "id")

	assert.NotEmpty(t, res, "[TestChiURLParamToString] Should not empty")
	assert.Equal(t, "9908-test", res, "[TestChiURLParamToString] Result should \"9908-test\"")
}

func TestChiURLParamToInt(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/9908", nil)

	ctx := context.WithValue(request.Context(), chi.RouteCtxKey, &chi.Context{
		URLParams: chi.RouteParams{
			Keys:   []string{"id"},
			Values: []string{"9908"},
		},
	})

	res := rhelper.ChiURLParamToInt(request.WithContext(ctx), "id", 0)

	assert.NotEmpty(t, res, "[TestChiURLParamToInt] Should not empty")
	assert.Equal(t, int(9908), res, "[TestChiURLParamToInt] Result should int(9908)")
}

func TestChiURLParamToInt64(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/9908", nil)

	ctx := context.WithValue(request.Context(), chi.RouteCtxKey, &chi.Context{
		URLParams: chi.RouteParams{
			Keys:   []string{"id"},
			Values: []string{"9908"},
		},
	})

	res := rhelper.ChiURLParamToInt64(request.WithContext(ctx), "id", 0)

	assert.NotEmpty(t, res, "[TestChiURLParamToInt64] Should not empty")
	assert.Equal(t, int64(9908), res, "[TestChiURLParamToInt64] Result should int64(9908)")
}

func TestChiURLParamToFloat64(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/9908", nil)

	ctx := context.WithValue(request.Context(), chi.RouteCtxKey, &chi.Context{
		URLParams: chi.RouteParams{
			Keys:   []string{"id"},
			Values: []string{"9908"},
		},
	})

	res := rhelper.ChiURLParamToFloat64(request.WithContext(ctx), "id", 0)

	assert.NotEmpty(t, res, "[TestChiURLParamToFloat64] Should not empty")
	assert.Equal(t, float64(9908), res, "[TestChiURLParamToFloat64] Result should float64(9908)")
}

func TestChiURLParamToFloat32(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/9908", nil)

	ctx := context.WithValue(request.Context(), chi.RouteCtxKey, &chi.Context{
		URLParams: chi.RouteParams{
			Keys:   []string{"id"},
			Values: []string{"9908"},
		},
	})

	res := rhelper.ChiURLParamToFloat32(request.WithContext(ctx), "id", 0)

	assert.NotEmpty(t, res, "[TestChiURLParamToFloat32] Should not empty")
	assert.Equal(t, float32(9908), res, "[TestChiURLParamToFloat32] Result should float32(9908)")
}

package rhelper_test

import (
	"net/http"
	"testing"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/rteja-library3/rhelper"
)

func TestRegisterPostEndpoint(t *testing.T) {
	rhelper.RegisterPostEndpoint(
		logrus.New(),
		chi.NewRouter(),
		func(rw http.ResponseWriter, r *http.Request) {},
		"/tes",
	)
}

func TestRegisterGetEndpoint(t *testing.T) {
	rhelper.RegisterGetEndpoint(
		logrus.New(),
		chi.NewRouter(),
		func(rw http.ResponseWriter, r *http.Request) {},
		"/tes",
	)
}

func TestRegisterPutEndpoint(t *testing.T) {
	rhelper.RegisterPutEndpoint(
		logrus.New(),
		chi.NewRouter(),
		func(rw http.ResponseWriter, r *http.Request) {},
		"/tes",
	)
}

func TestRegisterDeleteEndpoint(t *testing.T) {
	rhelper.RegisterDeleteEndpoint(
		logrus.New(),
		chi.NewRouter(),
		func(rw http.ResponseWriter, r *http.Request) {},
		"/tes",
	)
}

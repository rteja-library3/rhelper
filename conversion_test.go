package rhelper_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
)

func TestToInt64Success(t *testing.T) {
	res := rhelper.ToInt64("10", 1)

	exp := int64(10)

	assert.Equal(t, exp, res, "[TestToInt64Success] Result should int64(10)")
}

func TestToInt64WithDefault(t *testing.T) {
	res := rhelper.ToInt64("aa", 1)

	exp := int64(1)

	assert.Equal(t, exp, res, "[TestToInt64WithDefault] Result should int64(1)")
}

func TestToFloat64Success(t *testing.T) {
	res := rhelper.ToFloat64("100", 10)

	exp := float64(100)

	assert.Equal(t, exp, res, "[TestToFloat64Success] Result should float64(10)")
}

func TestToFloat64WithDefault(t *testing.T) {
	res := rhelper.ToFloat64("aa", 15)

	exp := float64(15)

	assert.Equal(t, exp, res, "[TestToFloat64WithDefault] Result should float64(15)")
}

func TestToFloat32Success(t *testing.T) {
	res := rhelper.ToFloat32("100", 10)

	exp := float32(100)

	assert.Equal(t, exp, res, "[TestToFloat32Success] Result should float32(10)")
}

func TestToFloat32WithDefault(t *testing.T) {
	res := rhelper.ToFloat32("aa", 15)

	exp := float32(15)

	assert.Equal(t, exp, res, "[TestToFloat32WithDefault] Result should float32(15)")
}

func TestToIntSuccess(t *testing.T) {
	res := rhelper.ToInt("10", 1)

	exp := int(10)

	assert.Equal(t, exp, res, "[TestToIntSuccess] Result should int(10)")
}

func TestToToIntWithMax(t *testing.T) {
	res := rhelper.ToIntWithMax("10", 2, 12)

	exp := int(10)

	assert.Equal(t, exp, res, "[TestToIntSuccess] Result should int(10)")
}

func TestToIntWithMinMax(t *testing.T) {
	res := rhelper.ToIntWithMinMax("aa", 2, 5, 12)

	exp := int(5)

	assert.Equal(t, exp, res, "[TestToIntSuccess] Result should int(5)")
}

func TestToIntWithMinMaxValidMax(t *testing.T) {
	res := rhelper.ToIntWithMinMax("aa", 15, 5, 12)

	exp := int(12)

	assert.Equal(t, exp, res, "[TestToIntSuccess] Result should int(12)")
}

package rhelper_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
)

func TestSearchArrayStringFoundLeft(t *testing.T) {
	ars := []string{"a", "b", "c"}
	s := "a"

	pos := rhelper.SearchInArrayString(ars, s)

	assert.Equal(t, 0, pos, "[TestSearchArrayStringFoundLeft] Position Should 0")
}

func TestSearchArrayStringFoundRight(t *testing.T) {
	ars := []string{"a", "b", "c"}
	s := "c"

	pos := rhelper.SearchInArrayString(ars, s)

	assert.Equal(t, 2, pos, "[TestSearchArrayStringFoundRight] Position Should 2")
}

func TestSearchArrayStringEmptyArray(t *testing.T) {
	ars := []string{}
	s := "c"

	pos := rhelper.SearchInArrayString(ars, s)

	assert.Equal(t, -1, pos, "[TestSearchArrayStringEmptyArray] Position Should -1")
}

func TestSearchArrayStringNotFound(t *testing.T) {
	ars := []string{"a", "b", "c"}
	s := "d"

	pos := rhelper.SearchInArrayString(ars, s)

	assert.Equal(t, -1, pos, "[TestSearchArrayStringFoundRight] Position Should -1")
}

func TestIsArrayStringExistFound(t *testing.T) {
	ars := []string{"a", "b", "c"}
	s := "a"

	isFound := rhelper.IsArrayStringExist(ars, s)

	assert.Equal(t, true, isFound, "[TestIsArrayStringExistFound] Should true")
}

func TestIsArrayStringExistNotFound(t *testing.T) {
	ars := []string{"a", "b", "c"}
	s := "d"

	isFound := rhelper.IsArrayStringExist(ars, s)

	assert.Equal(t, false, isFound, "[TestIsArrayStringExistNotFound] Should false")
}

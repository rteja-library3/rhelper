package rhelper_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rhelper"
	"go.mongodb.org/mongo-driver/bson"
)

func TestToMongoSort(t *testing.T) {
	sorts := []string{
		"-last_name",
		"dob",
	}

	exp := bson.D{
		{Key: "last_name", Value: -1},
		{Key: "dob", Value: 1},
	}

	res := rhelper.ToMongoSort(sorts)

	assert.NotEmpty(t, res, "[TestToMongoSort] Should not empty")
	assert.Equal(t, exp, res, "[TestToMongoSort] Result should equal")
}

func TestToMongoSortEmpty(t *testing.T) {
	sorts := []string{}

	exp := bson.D{}

	res := rhelper.ToMongoSort(sorts)

	assert.Empty(t, res, "[TestToMongoSort] Should empty")
	assert.Equal(t, exp, res, "[TestToMongoSort] Result should equal")
}
